# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  comp_ans = (1..100).to_a.shuffle.first
  hum_ans = 0
  guesses = []

  until won?(comp_ans, hum_ans)
    puts 'Guess a number'
    print hum_ans = gets.chomp.to_i
    print " is "
    guesses << hum_ans
    puts comparison(comp_ans, hum_ans)
    raise NoMoreInput if hum_ans == 0
    print 'Number of guesses: '
    puts guesses.length
  end
  print 'Congrats, you guessed the right number, which was: '
  puts hum_ans
end

def comparison(comp_ans, hum_ans)
  return 'too low' if hum_ans < comp_ans
  return 'too high' if hum_ans > comp_ans
  'correct!'
end

def won?(ans_one, ans_two)
  return true if ans_one == ans_two
  false
end

def shuffle
  puts 'Please provide a text file which you would like to shuffle?'
  file = gets.chomp
  shuffled_content = File.readlines(file).shuffle
  File.open("{#{file}}-shuffled.txt", "w") do |f|
    shuffled_content.each {|line| f.puts line}
  end
end

def intro
  puts 'Which program would you like to use? Please choose one: guessing or shuffle)'
  answer = gets.chomp
  choice(answer)
end

def choice(answer)
  if answer == "guessing"
    guessing_game
  elsif answer == "shuffle"
    shuffle
  else
    puts 'Invalid answer!'
    intro
  end
end

if __FILE__ == $PROGRAM_NAME
  intro
end
